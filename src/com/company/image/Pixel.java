package com.company.image;

public class Pixel {

    private byte r,g,b;

    private double a;

    Pixel(byte r, byte g, byte b, double a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    public byte getR() {
        return r;
    }

    public void setR(byte r) {
        this.r = r;
    }

    public byte getG() {
        return g;
    }

    public void setG(byte g) {
        this.g = g;
    }

    public byte getB() {
        return b;
    }

    public void setB(byte b) {
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    @Override
    public String toString() {
        return "Pixel{" +
                "R=" + r +
                ", G=" + g +
                ", B=" + b +
                ", A=" + a +
                '}';
    }
}

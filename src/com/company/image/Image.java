package com.company.image;

public class Image {
    private int width;
    private int height;
    private Pixel[][] bitmap;

    public Image() {
        this.width = 0;
        this.height = 0;
        this.bitmap = new Pixel[height][width];
    }

    public Image(int width, int height) {
        this.width = width;
        this.height = height;
        this.bitmap = new Pixel[height][width];
    }

    public Pixel[][] getBitmap() {
        return bitmap;
    }

    public void setBitmap(Pixel[][] bitmap) {
        this.bitmap = bitmap;
    }

    public void setRandomImage() {
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                bitmap[i][j] = new Pixel((byte) 50, (byte) 50, (byte) 50, 0);
            }
        }
    }

}

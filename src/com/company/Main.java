package com.company;

import com.company.filter.FilterBuilder;
import com.company.filter.FilterFactory;
import com.company.filter.FilterTypes;
import com.company.image.Image;
import com.company.image.Pixel;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Image img = new Image(50, 50);
        img.setRandomImage();

        FilterBuilder filterBuilder = new FilterBuilder();
        img = filterBuilder.init(img).enableBlurFilter().enableContrastFilter().enableNoiseFilter().enableScaleFilter().toFilter(); //builder realization


        Image img2 = new Image(50, 50);
        img2.setRandomImage();

        FilterFactory filterFactory = new FilterFactory();                                                                          //factory realization
        img2 = filterFactory.create(FilterTypes.Noise).filtrate(img);

        for (Pixel[] pixels : img2.getBitmap()) {
            System.out.println(Arrays.toString(pixels));
        }
    }
}

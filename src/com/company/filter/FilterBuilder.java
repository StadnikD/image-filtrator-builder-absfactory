package com.company.filter;

import com.company.image.Image;

//Builder pattern
public class FilterBuilder {
    private boolean blurFilter = false;
    private boolean contrastFilter = false;
    private boolean noiseFilter = false;
    private boolean scaleFilter = false;

    private Image instance;

    public FilterBuilder enableBlurFilter() {
        this.blurFilter = true;
        return this;
    }

    public FilterBuilder enableContrastFilter() {
        this.contrastFilter = true;
        return this;
    }

    public FilterBuilder enableNoiseFilter() {
        this.noiseFilter = true;
        return this;
    }

    public FilterBuilder enableScaleFilter() {
        this.scaleFilter = true;
        return this;
    }

    public FilterBuilder init(Image instance) {
        this.instance = instance;
        return this;
    }

    public Image toFilter() {
        if (blurFilter)
            instance = new BlurFilter().filtrate(instance);
        if (contrastFilter)
            instance = new ContrastFilter().filtrate(instance);
        if (noiseFilter)
            instance = new NoiseFilter().filtrate(instance);
        if (scaleFilter)
            instance = new ScaleFilter().filtrate(instance);
        return instance;
    }
}

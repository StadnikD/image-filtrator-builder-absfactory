package com.company.filter;

public enum FilterTypes {
    Blur,
    Contrast,
    Noise,
    Scale
}

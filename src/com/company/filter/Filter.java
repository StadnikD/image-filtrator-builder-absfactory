package com.company.filter;

import com.company.image.Image;
import com.company.image.Pixel;


//Abstract method
public abstract class Filter implements Filtrate {

    public Image filtrate(Image img) {
        Pixel[][] pixels = img.getBitmap();
        for (Pixel[] pixel : pixels) {
            for (Pixel aPixel : pixel) {
                aPixel = changePixel(aPixel);
            }
        }
        img.setBitmap(pixels);
        return img;
    }

     protected abstract Pixel changePixel(Pixel pixel);
}

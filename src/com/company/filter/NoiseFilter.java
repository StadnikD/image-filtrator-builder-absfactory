package com.company.filter;

import com.company.image.Pixel;

public class NoiseFilter extends Filter {

//    NEED TO ADD TRY/CATCH WITH EXCEPTION

    @Override
    protected Pixel changePixel(Pixel pixel) {
        pixel.setR((byte) (pixel.getR() + 2));
        pixel.setG((byte) (pixel.getG() + 2));
        pixel.setB((byte) (pixel.getB() + 2));
        return pixel;
    }
}

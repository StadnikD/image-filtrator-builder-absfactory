package com.company.filter;


//Abstract factory
public class FilterFactory {

    public Filtrate create(FilterTypes filterTypes) {
        switch (filterTypes) {
            case Blur:
                return new BlurFilter();
            case Noise:
                return new NoiseFilter();
            case Scale:
                return new ScaleFilter();
            case Contrast:
                return new ContrastFilter();
            default:
                throw new IllegalArgumentException("not implemented");
        }
    }
}

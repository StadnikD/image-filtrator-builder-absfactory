package com.company.filter;

import com.company.image.Image;

//Command pattern
public interface Filtrate {
    Image filtrate(Image img);
}

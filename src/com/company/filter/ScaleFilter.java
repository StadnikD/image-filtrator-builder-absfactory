package com.company.filter;

import com.company.image.Pixel;

public class ScaleFilter extends Filter {

    @Override
    protected Pixel changePixel(Pixel pixel) {
        pixel.setR((byte) 0);
        pixel.setG((byte) 0);
        pixel.setB((byte) 0);
        pixel.setA((byte) 0.8);
        return pixel;
    }
}

package com.company.filter;

import com.company.image.Pixel;

public class BlurFilter extends Filter {

    @Override
    protected Pixel changePixel(Pixel pixel) {
        pixel.setA(0.5);
        return pixel;
    }
}
